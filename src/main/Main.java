package main;

import easyJava.classes.TestLivre;
import easyJava.classes.TestRectangle;
import easyJava.classes.TestTemps;

public class Main {

	public static void main(String[] args) {
		TestLivre.run();
		TestTemps.run();
		TestRectangle.run();
	}

}
