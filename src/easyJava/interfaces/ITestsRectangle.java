package easyJava.interfaces;

import easyJava.classes.Rectangle;

public interface ITestsRectangle {
	public boolean createdWithoutArguments();
	public boolean createdWithArguments(double longueur, double largeur);
	public boolean createdWithRectangle(Rectangle rectangle);
	public boolean testPerimetre(double longueur, double largeur);
	public boolean testAire(double longueur, double largeur);
	public boolean testCarre(double longueur, double largeur);
}
