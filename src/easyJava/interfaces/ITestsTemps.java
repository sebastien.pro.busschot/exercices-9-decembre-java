package easyJava.interfaces;

import easyJava.classes.Temps;

public interface ITestsTemps {
	public boolean createdWhitoutArguments();
	public boolean createdWhitHours(int hours);
	public boolean createdWhitHoursAndMinutes(int hours, int minutes);
	public boolean createdWhitHoursAndMinutesAndSeconds(int hours, int minutes, int seconds);
	public boolean createdWhitTempsObject(Temps temps);
	public boolean setIncorrectValueHourAsImpossible(int hours);
	public boolean setIncorrectValueMinutesAsImpossible(int minutes);
	public boolean setIncorrectValueSecondsAsImpossible(int seconds);
	public boolean addHours();
	public boolean addMinutes();
	public boolean addSeconds();
}
