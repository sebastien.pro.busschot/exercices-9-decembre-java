package easyJava.interfaces;

public interface ITemps {
	public void ajouterHeures(int heures);
	public void ajouterMinutes(int minutes);
	public void ajouterSecondes(int secondes);
}
