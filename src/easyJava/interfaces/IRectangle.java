package easyJava.interfaces;

public interface IRectangle {

	public double perimetre();
	public double aire();
	public boolean isCarre();

}
