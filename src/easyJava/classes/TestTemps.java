package easyJava.classes;

import easyJava.interfaces.ITestsTemps;

public class TestTemps implements ITestsTemps{

	public static void run() {
		boolean result = true;
		
		TestTemps test = new TestTemps();
		
		if(!test.createdWhitoutArguments()) {
			System.out.println("Error creation without arguments");
			result = false;
		}
		
		if(!test.createdWhitHours(12)) {
			System.out.println("Error creation with hours");
			result = false;
		}
		
		if(!test.createdWhitHoursAndMinutes(12, 12)) {
			System.out.println("Error creation with hours and minutes");
			result = false;
		}
		
		if(!test.createdWhitHoursAndMinutesAndSeconds(12, 12, 12)) {
			System.out.println("Error creation with hours, minutes and seconds");
			result = false;
		}
		
		if(!test.createdWhitTempsObject(new Temps(12,12,12))) {
			System.out.println("Error creation with Temps object");
			result = false;
		}
		
		if(!test.setIncorrectValueHourAsImpossible(50) && !test.setIncorrectValueHourAsImpossible(-3)) {
			System.out.println("Error makes incorrect hours values is possible");
			result = false;
		}
		
		if(!test.setIncorrectValueMinutesAsImpossible(70) && !test.setIncorrectValueMinutesAsImpossible(-3)) {
			System.out.println("Error makes incorrect minutes values is possible");
			result = false;
		}
		
		if(!test.setIncorrectValueSecondsAsImpossible(70) && !test.setIncorrectValueSecondsAsImpossible(-3)) {
			System.out.println("Error makes incorrect seconds values is possible");
			result = false;
		}
		
		result = test.addHours() ? result : false;
		result = test.addMinutes() ? result : false;
		result = test.addSeconds() ? result : false;
		
		if(result) {
			System.out.println("All test on TestTemps.run() as positively passed");
		}
		
	}

	@Override
	public boolean createdWhitoutArguments() {
		Temps temps = new Temps();
		
		if(temps.getHeures() == 0 && temps.getMinutes() == 0 && temps.getSecondes() == 0) {
			return true;
		}
		
		return false;
	}

	@Override
	public boolean createdWhitHours(int hours) {
		Temps temps = new Temps(hours);
		
		if(temps.getHeures() == hours && temps.getMinutes() == 0 && temps.getSecondes() == 0) {
			return true;
		}
		
		return false;
	}

	@Override
	public boolean createdWhitHoursAndMinutes(int hours, int minutes) {
		Temps temps = new Temps(hours,minutes);
		
		if(temps.getHeures() == hours && temps.getMinutes() == minutes && temps.getSecondes() == 0) {
			return true;
		}
		
		return false;
	}

	@Override
	public boolean createdWhitHoursAndMinutesAndSeconds(int hours, int minutes, int seconds) {
		Temps temps = new Temps(hours,minutes,seconds);
		
		if(temps.getHeures() == hours && temps.getMinutes() == minutes && temps.getSecondes() == seconds) {
			return true;
		}
		
		return false;
	}

	@Override
	public boolean createdWhitTempsObject(Temps temps) {
		Temps testTemps = new Temps(temps);
		
		if(
			testTemps.getHeures() == temps.getHeures() &&
			testTemps.getMinutes() == temps.getMinutes() &&
			testTemps.getSecondes() == temps.getSecondes()
		) {
			return true;
		}
		
		return false;
	}

	@Override
	public boolean setIncorrectValueHourAsImpossible(int hours) {
		Temps temps = new Temps(hours);
		
		if(temps.getHeures() < 24 &&  temps.getHeures() >= 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean setIncorrectValueMinutesAsImpossible(int minutes) {
		Temps temps = new Temps();
		temps.setMinutes(minutes);

		if(temps.getMinutes() < 60 &&  temps.getMinutes() >= 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean setIncorrectValueSecondsAsImpossible(int secondes) {
		Temps temps = new Temps();
		temps.setSecondes(secondes);

		if(temps.getSecondes() < 60 &&  temps.getSecondes() >= 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean addHours() {
		boolean test = true;
		Temps temps = new Temps();
		
		temps.ajouterHeures(3);
		
		if (temps.getHeures() != 3) {
			test = false;
			System.out.println("Error on add 3h to 0h => "+temps.getHeures()+" attemps 3");
		}
	
		temps.ajouterHeures(26);
		
		if (temps.getHeures() != 5) {
			test = false;
			System.out.println("Error on add 26h to 3h=> "+temps.getHeures()+" attemps 5");
		}

		temps.ajouterHeures(240);
		
		if (temps.getHeures() != 5) {
			test = false;
			System.out.println("Error on add 240h to 5h=> "+temps.getHeures()+" attemps 5");
		}
		
		return test;
	}

	@Override
	public boolean addMinutes() {
		boolean test = true;
		Temps temps = new Temps();
		
		temps.ajouterMinutes(3);
		
		if (temps.getMinutes() != 3) {
			test = false;
			System.out.println("Error on add 3m to 0m => "+temps.getMinutes()+" attemps 3");
		}
	
		temps.ajouterMinutes(70);
		
		if (temps.getMinutes() != 13) {
			test = false;
			System.out.println("Error on add 70m to 13m=> "+temps.getMinutes()+" attemps 13");
		}

		temps.ajouterMinutes(600);
		
		if (temps.getMinutes() != 13) {
			test = false;
			System.out.println("Error on add 600m to 13m=> "+temps.getMinutes()+" attemps 13");
		}
		
		return test;
	}

	@Override
	public boolean addSeconds() {
		boolean test = true;
		Temps temps = new Temps();
		
		temps.ajouterSecondes(3);
		
		if (temps.getSecondes() != 3) {
			test = false;
			System.out.println("Error on add 3m to 0m => "+temps.getSecondes()+" attemps 3");
		}
	
		temps.ajouterSecondes(70);
		
		if (temps.getSecondes() != 13) {
			test = false;
			System.out.println("Error on add 70m to 13m=> "+temps.getSecondes()+" attemps 13");
		}

		temps.ajouterSecondes(600);
		
		if (temps.getSecondes() != 13) {
			test = false;
			System.out.println("Error on add 600m to 13m=> "+temps.getSecondes()+" attemps 13");
		}
		
		return test;
	}

	

}
