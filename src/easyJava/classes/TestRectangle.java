package easyJava.classes;

import easyJava.interfaces.ITestsRectangle;
import easyJava.classes.Rectangle;

public class TestRectangle implements ITestsRectangle{

	public static void run() {
		boolean result = true;
		TestRectangle test = new TestRectangle();
		
		if(!test.createdWithoutArguments()) {
			result = false;
			System.out.println("Error creation without arguments");
		}
		
		if(!test.createdWithArguments(12, 14)) {
			result = false;
			System.out.println("Error creation with length and width arguments");
		}
		
		if(!test.createdWithRectangle(new Rectangle(12,12))) {
			result = false;
			System.out.println("Error creation with rectangle argument");
		}
		
		if(!test.testPerimetre(12, 12)) {
			result = false;
			System.out.println("Error test perimeter");
		}
		
		if(!test.testAire(12, 12)) {
			result = false;
			System.out.println("Error test area");
		}
		
		if(!test.testCarre(12, 12)) {
			result = false;
			System.out.println("Error test is square");
		}
		
		if(result) {
			System.out.println("All test on TestRectangle.run() as positively passed");
		}
		
		Incrementator.inc(3);
		Incrementator.inc("My taylor is rich ");
		Incrementator.inc(new Rectangle(12,12));
	}

	@Override
	public boolean createdWithoutArguments() {
		Rectangle rectangle = new Rectangle();
		if(rectangle.getLongeur() == 0 && rectangle.getLargeur() == 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean createdWithArguments(double longueur, double largeur) {
		Rectangle rectangle = new Rectangle(longueur, largeur);
		if(rectangle.getLongeur() == longueur && rectangle.getLargeur() == largeur) {
			return true;
		}
		return false;
	}

	@Override
	public boolean createdWithRectangle(Rectangle rectangle) {
		Rectangle newRectangle = new Rectangle(rectangle.getLongeur(), rectangle.getLargeur());
		if(rectangle.getLongeur() == newRectangle.getLongeur() && rectangle.getLargeur() == newRectangle.getLargeur()) {
			return true;
		}
		return false;
	}

	@Override
	public boolean testPerimetre(double longueur, double largeur) {
		Rectangle rectangle = new Rectangle(longueur, largeur);
		if(rectangle.perimetre() == 2*(longueur+largeur)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean testAire(double longueur, double largeur) {
		Rectangle rectangle = new Rectangle(longueur, largeur);
		if(rectangle.aire() == longueur*largeur) {
			return true;
		}
		return false;
	}

	@Override
	public boolean testCarre(double longueur, double largeur) {
		Rectangle rectangle = new Rectangle(longueur, largeur);
		boolean isCarre = longueur == largeur ? true : false;
		if(rectangle.isCarre() == isCarre) {
			return true;
		}
		return false;
	}

	
}
