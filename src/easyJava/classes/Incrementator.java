package easyJava.classes;

public class Incrementator {
	
	public static void inc(int integer) {
		System.out.print("Get "+integer);
		integer++;
		System.out.println(": add 1 to integer : "+integer);
	}
	
	public static void inc(String string) {
		System.out.print("Get '"+string+"'");
		string += "O";
		System.out.println(": add O to string : '"+string+"'");
	}
	
	public static void inc(Rectangle rectangle) {
		double lenght = rectangle.getLongeur();
		double width = rectangle.getLargeur();
		
		System.out.print("Get rectangle as "+lenght+" to length and "+width+" to width");
		rectangle.setLongeur(++lenght);
		rectangle.setLargeur(++width);
		System.out.println(" and set rectangle as "+lenght+" to length and "+width+" to width");
	}
}
