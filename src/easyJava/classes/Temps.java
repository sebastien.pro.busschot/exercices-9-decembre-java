package easyJava.classes;

import easyJava.interfaces.ITemps;

public class Temps implements ITemps {
	
	private int heures = 0;
	private int minutes = 0;
	private int secondes = 0;

	public int getHeures() {
		return heures;
	}

	public void setHeures(int heures) {
		if(heures < 24 && heures >= 0) {
			this.heures = heures;
		}
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		if(minutes < 60 && minutes >=0) {
			this.minutes = minutes;			
		}
	}

	public int getSecondes() {
		return secondes;
	}

	public void setSecondes(int secondes) {
		if(secondes < 60 && secondes >=0) {		
			this.secondes = secondes;
		}
	}

	@Override
	public String toString() {
		return "Temps [heures=" + heures + ", minutes=" + minutes + ", secondes=" + secondes + "]";
	}
	
	// CONSTRUCTEURS

	public Temps() {}
	
	
	public Temps(int heures) {
		this.setHeures(heures);
	}
	
	public Temps(int heures, int minutes) {
		this.setHeures(heures);
		this.setMinutes(minutes);
	}
	
	public Temps(int heures, int minutes, int secondes) {
		this.setHeures(heures);
		this.setMinutes(minutes);
		this.setSecondes(secondes);
	}
	
	public Temps(Temps temps) {
		this.setHeures(temps.getHeures());
		this.setMinutes(temps.getMinutes());
		this.setSecondes(temps.getSecondes());
	}

	@Override
	public void ajouterHeures(int heures) {
		int heuresCount = this.heures + heures;
		this.heures = heuresCount > 23	? heuresCount % 24 : heuresCount;	
	}

	@Override
	public void ajouterMinutes(int minutes) {
		int minutesCount = this.minutes + minutes;
		this.minutes = minutesCount > 59 ? minutesCount % 60 : minutesCount;
		
	}

	@Override
	public void ajouterSecondes(int secondes) {
		int secondsCount = this.secondes + secondes;
		this.secondes = secondsCount > 59 ? secondsCount % 60 : secondsCount;
	}

}
