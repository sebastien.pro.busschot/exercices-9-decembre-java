package easyJava.classes;

public class TestLivre {

	public static void run() {
		final String titre = "Tintin, l'autobiographie";
		final String autheur = "Tintin";
		final double prix = 1.50;
		final int annee = 2022;
		
		Livre livre = new Livre(titre, autheur, prix, annee);
		
		if(
			livre.getTitle().equals(titre) &&
			livre.getAuthor().equals(autheur) &&
			livre.getPrice() == prix &&
			livre.getParutionYear() == annee
				) {
			System.out.println("All test on TestLivre.run() as positively passed");
		} else {
			System.out.println("Error, Livre doesen't content valid datas");
		}
	}

}
