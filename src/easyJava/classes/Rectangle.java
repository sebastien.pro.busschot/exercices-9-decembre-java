package easyJava.classes;

import easyJava.interfaces.IRectangle;

public class Rectangle implements IRectangle{
	
	private double longeur = 0;
	private double largeur = 0;

	public Rectangle() {}
	
	public Rectangle(double longeur, double largeur) {
		this.setLongeur(longeur);
		this.setLargeur(largeur);
	}
	
	public Rectangle(Rectangle rectangle) {
		this.setLongeur(rectangle.getLongeur());
		this.setLargeur(rectangle.getLargeur());
	}

	@Override
	public double perimetre() {
		return 2*(this.getLongeur()+this.getLargeur());
	}

	@Override
	public double aire() {
		return this.getLongeur()*this.getLargeur();
	}

	@Override
	public boolean isCarre() {
		return this.getLongeur() == this.getLargeur() ? true : false;
	}

	@Override
	public String toString() {
		return "Rectangle [longeur=" + longeur + ", largeur=" + largeur + "]";
	}

	public double getLongeur() {
		return longeur;
	}

	public void setLongeur(double longeur) {
		this.longeur = longeur;
	}

	public double getLargeur() {
		return largeur;
	}

	public void setLargeur(double largeur) {
		this.largeur = largeur;
	}

}
