package easyJava.classes;

public class Livre {
	private String title;
	private String author;
	private double price;
	private int parutionYear;
	
	public Livre(String title, String author, double price, int parutionYear) {
		this.title = title;
		this.author = author;
		this.price = price;
		this.parutionYear = parutionYear;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getParutionYear() {
		return parutionYear;
	}

	public void setParutionYear(int parutionYear) {
		this.parutionYear = parutionYear;
	}

	@Override
	public String toString() {
		return "Livre [title=" + title + ", author=" + author + ", price=" + price + ", parutionYear=" + parutionYear
				+ "]";
	}
	
	
}
